resource "tls_private_key" "global_key" {
  algorithm   = var.algorithm
  rsa_bits    = local.rsa_bits
  ecdsa_curve = local.ecdsa_curve
}

resource "hcloud_ssh_key" "ssh_key" {
  name       = var.ssh_key_name
  public_key = tls_private_key.global_key.public_key_openssh
}
