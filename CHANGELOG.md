## [1.0.1](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-security/compare/1.0.0...1.0.1) (2022-12-15)


### Bug Fixes

* added linting, pipeline and tests ([bca897d](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-security/commit/bca897db2a79549be49297de8f3a3d0b007826bd))
* added ssh tests ([e9ab043](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-security/commit/e9ab04322d87f325a72a40609b5ef243c3839e2c))
