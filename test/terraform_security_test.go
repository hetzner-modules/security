package test

import (
	"fmt"
	"os"
	"testing"

	"github.com/gruntwork-io/terratest/modules/random"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

func TestNetwork(t *testing.T) {
	t.Parallel()

	terraformOptions := &terraform.Options{
		TerraformDir: "../examples",
		VarFiles:     []string{"terraform.tfvars"},
		Vars: map[string]interface{}{
			"ssh_key_name": fmt.Sprintf("ssh-key-%s", random.UniqueId()),
		},
		BackendConfig: map[string]interface{}{
			"username": os.Getenv("TF_USERNAME"),
			"password": os.Getenv("ACCESS_TOKEN"),
		},
	}

	defer terraform.Destroy(t, terraformOptions)
	terraform.InitAndApply(t, terraformOptions)
	ssh_key_id := terraform.Output(t, terraformOptions, "ssh_key_id")
	private_key_pem := terraform.Output(t, terraformOptions, "private_key_pem")
	public_key_openssh := terraform.Output(t, terraformOptions, "public_key_openssh")

	assert.NotEmpty(t, ssh_key_id, "SSH key successfully created")
	assert.NotEmpty(t, private_key_pem, "Private key successfully created")
	assert.NotEmpty(t, public_key_openssh, "Public SSH key successfully created")
}
