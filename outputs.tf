output "ssh_key_id" {
  description = "(int) The unique ID of the key."
  value       = hcloud_ssh_key.ssh_key.id
  sensitive   = true
}

output "private_key_pem" {
  description = "(String, Sensitive) Private key data in PEM (RFC 1421) format."
  value       = tls_private_key.global_key.private_key_pem
  sensitive   = true
}

output "public_key_openssh" {
  description = "(String) The public key data in 'Authorized Keys' format. This is not populated for ECDSA with curve P224, as it is not supported. NOTE: the underlying libraries that generate this value append a \n at the end of the PEM. In case this disrupts your use case, we recommend using trimspace()."
  value       = tls_private_key.global_key.public_key_openssh
  sensitive   = true
}
