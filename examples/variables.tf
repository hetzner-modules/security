variable "ssh_key_name" {
  type        = string
  description = "name usd for the temporary ssh key"
}

variable "algorithm" {
  type        = string
  description = "(String) Name of the algorithm to use when generating the private key. Currently-supported values are: RSA, ECDSA, ED25519."
  default     = "RSA"
}

variable "rsa_bits" {
  type        = number
  description = "(Number) When algorithm is RSA, the size of the generated RSA key, in bits (default: 2048)."
  default     = 2048
}

variable "ecdsa_curve" {
  type        = string
  description = "(String) When algorithm is ECDSA, the name of the elliptic curve to use. Currently-supported values are: P224, P256, P384, P521. (default: P224)."
  default     = "P224"
}

variable "hcloud_token" {
  sensitive   = true
  type        = string
  description = "Hetzner Cloud API token used to create infrastructure"
}