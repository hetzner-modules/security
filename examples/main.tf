terraform {
  required_version = ">= 1.2.0"
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/52559957/terraform/state/security"
    lock_address   = "https://gitlab.com/api/v4/projects/52559957/terraform/state/security/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/52559957/terraform/state/security/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
  }

  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.36.1"
    }
  }
}

provider "hcloud" {
  token = var.hcloud_token
}

module "security" {
  source       = "../"
  ssh_key_name = var.ssh_key_name
  algorithm    = var.algorithm
  ecdsa_curve  = var.ecdsa_curve
  rsa_bits     = var.rsa_bits
}